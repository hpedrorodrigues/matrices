CC=g++
CFLAGS=-c -Wall
SOURCES=src/main.cpp src/run/Runner.cpp src/model/Matrix.cpp src/model/MatrixManager.cpp src/model/MatrixOperations.cpp src/model/Constants.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=matrices

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) ; $(CC) $(OBJECTS) -o $@

.cpp.o: ; $(CC) $(CFLAGS) $< -o $@

clean: ; rm $(OBJECTS)