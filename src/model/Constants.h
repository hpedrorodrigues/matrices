#ifndef MATRICES_CONSTANTS_H
#define MATRICES_CONSTANTS_H

class Constants {

public:

    class Option {

    public:

        static const int SUM = 1;
        static const int SUBTRACTION = 2;
        static const int MULTIPLICATION = 3;
        static const int INVERSE = 4;
        static const int EXIT = 5;
    };
};

#endif