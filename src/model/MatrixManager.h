#ifndef MATRICES_MATRIXMANAGER_H
#define MATRICES_MATRIXMANAGER_H

#include <iostream>

using namespace std;

class MatrixManager {

public:

    void readMatrix(int rowsCount, int columnsCount, int **matrix);

    void readMatrix(int order, int **matrix);

    void printMatrix(int rowsCount, int columnsCount, int **matrix);

    void printMatrix(int order, int **matrix);

    void deleteMatrix(int rowsCount, int columnsCount, int **matrix);

    void deleteMatrix(int order, int **matrix);
};

#endif