#ifndef MATRICES_MATRIX_H
#define MATRICES_MATRIX_H

#include <iostream>

class Matrix {

private:

    int columnCount, rowCount;
    int **matrix;

public:

    Matrix();

    ~Matrix();

    int getColumnCount();

    void setColumnCount(int columnCount);

    int getRowCount();

    void setRowCount(int rowCount);

    void create();

    int **getMatrix();
};

#endif