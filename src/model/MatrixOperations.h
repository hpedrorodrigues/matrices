#ifndef MATRICES_MATRIXOPERATIONS_H
#define MATRICES_MATRIXOPERATIONS_H

#include "Matrix.h"

class MatrixOperations {

private:

    Matrix *firstMatrix, *secondMatrix;

    double getDeterminant(int **matrix, int n);

public:

    MatrixOperations();

    ~MatrixOperations();

    Matrix *getFirstMatrix();

    void setFirstMatrix(Matrix *firstMatrix);

    Matrix *getSecondMatrix();

    void setSecondMatrix(Matrix *secondMatrix);

    bool canSum();

    bool canMultiply();

    bool canInverse();

    bool canSubtract();

    int **getSum();

    int **getMultiplication();

    int **getInversion();

    int **getSubtraction();

    double getDeterminant();
};

#endif