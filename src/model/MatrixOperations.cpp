#include <math.h>
#include "MatrixOperations.h"

MatrixOperations::MatrixOperations() {
}

MatrixOperations::~MatrixOperations() {
}

Matrix *MatrixOperations::getFirstMatrix() {
    return this->firstMatrix;
}

void MatrixOperations::setFirstMatrix(Matrix *firstMatrix) {
    this->firstMatrix = firstMatrix;
}

Matrix *MatrixOperations::getSecondMatrix() {
    return this->secondMatrix;
}

void MatrixOperations::setSecondMatrix(Matrix *secondMatrix) {
    this->secondMatrix = secondMatrix;
}

/**
 * Adição
 *
 * Para adicionarmos duas ou mais matrizes é preciso que todas elas tenham o mesmo número de linhas e de colunas.
 * A soma dessas matrizes irá resultar em outra matriz que também terá o mesmo número de linhas e de colunas.
 *
 * Os termos deverão ser somados com os seus termos correspondentes.
 *
 * Concluímos que:
 * Dada duas matrizes, A e B, as duas de ordem m x n. Então, A + B = C, com C de ordem m x n ↔ a11 + b11 = c11.
 */
bool MatrixOperations::canSum() {
    return this->firstMatrix->getRowCount() == this->secondMatrix->getRowCount()
           && this->firstMatrix->getColumnCount() == this->secondMatrix->getColumnCount();
}

/**
 * Multiplicação
 *
 * Para realizar uma multiplicação entre duas matrizes, é necessário que o número de colunas da primeira matriz seja
 * igual ao número de linhas da segunda matriz. O resultado dessa multiplicação será uma matriz com o número de linhas
 * da primeira e o número de colunas da segunda.
 *
 * A m x n . B n x p = C m x p
 */
bool MatrixOperations::canMultiply() {
    return this->firstMatrix->getColumnCount() == this->secondMatrix->getRowCount();
}

bool MatrixOperations::canInverse() {
    return this->getDeterminant() != 0;
}

/**
 * Subtração
 *
 * Para efetuarmos a subtração de duas matrizes, as matrizes subtraídas devem ter a mesma ordem
 * (mesmo número de linhas e colunas) e a matriz obtida com a subtração (matriz diferença) também deve ter o mesmo
 * número de linhas e colunas que as matrizes subtraídas.
 *
 * Cada elemento de uma matriz deve ser subtraído com o elemento correspondente da outra matriz.
 *
 * Concluímos que:
 * Dada duas matrizes, A e B, as duas de ordem m x n. Então A – B = C de ordem m x n ↔ a11 – b11 = c11.
 */
bool MatrixOperations::canSubtract() {
    return this->canSum();
}

int **MatrixOperations::getSum() {
    int **matrixResult = new int *[this->firstMatrix->getRowCount()];

    for (register int r = 0; r < this->firstMatrix->getRowCount(); r++) {

        matrixResult[r] = new int[this->secondMatrix->getColumnCount()];

        for (register int c = 0; c < this->secondMatrix->getColumnCount(); c++) {

            matrixResult[r][c] = this->firstMatrix->getMatrix()[r][c] + this->secondMatrix->getMatrix()[r][c];
        }
    }

    return matrixResult;
}

/**
 * Realiza a multiplicação entre as matrizes.
 *
 * Lembrando que no loop mais interno poderia ser
 * `this->secondMatrix->getRowCount()` ou
 * `this->firstMatrix->getColumnCount()` já que são iguais.
 */
int **MatrixOperations::getMultiplication() {
    int **matrixResult = new int *[this->firstMatrix->getRowCount()];

    for (register int r = 0; r < this->firstMatrix->getRowCount(); r++) {

        matrixResult[r] = new int[this->secondMatrix->getColumnCount()];

        for (register int c = 0; c < this->secondMatrix->getColumnCount(); c++) {

            int value = 0;

            for (register int i = 0; i < this->secondMatrix->getRowCount(); i++) {

                value += this->firstMatrix->getMatrix()[r][i] * this->secondMatrix->getMatrix()[i][c];
            }

            matrixResult[r][c] = value;
        }
    }

    return matrixResult;
}

int **MatrixOperations::getInversion() {
    return NULL;
}

int **MatrixOperations::getSubtraction() {
    int **matrixResult = new int *[this->firstMatrix->getRowCount()];

    for (register int r = 0; r < this->firstMatrix->getRowCount(); r++) {

        matrixResult[r] = new int[this->secondMatrix->getColumnCount()];

        for (register int c = 0; c < this->secondMatrix->getColumnCount(); c++) {

            matrixResult[r][c] = this->firstMatrix->getMatrix()[r][c] - this->secondMatrix->getMatrix()[r][c];
        }
    }

    return matrixResult;
}

double MatrixOperations::getDeterminant() {
    int order = this->firstMatrix->getRowCount();

    return this->getDeterminant(this->firstMatrix->getMatrix(), order);
}

double MatrixOperations::getDeterminant(int **matrix, int n) {
    int j2;
    double determinant = 0;
    int **auxiliarMatrix = NULL;

    if (n < 1) {

        /* Error */
    } else if (n == 1) {

        determinant = matrix[0][0];
    } else if (n == 2) {

        determinant = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    } else {

        determinant = 0;

        for (int j1 = 0; j1 < n; j1++) {

            auxiliarMatrix = new int *[n - 1];

            for (int i = 0; i < n - 1; i++) {

                auxiliarMatrix[i] = new int[n - 1];
            }

            for (int i = 1; i < n; i++) {

                j2 = 0;

                for (int j = 0; j < n; j++) {

                    if (j != j1) {

                        auxiliarMatrix[i - 1][j2] = matrix[i][j];
                        j2++;
                    }
                }
            }

            determinant += pow(-1.0, 1.0 + j1 + 1.0) * matrix[0][j1] * getDeterminant(auxiliarMatrix, n - 1);

            for (int i = 0; i < n - 1; i++) {
                delete auxiliarMatrix[i];
            }

            delete auxiliarMatrix;
        }
    }

    return determinant;
}