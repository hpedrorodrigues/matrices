#include "MatrixManager.h"

void MatrixManager::readMatrix(int order, int **matrix) {

    this->readMatrix(order, order, matrix);
}

void MatrixManager::readMatrix(int rowsCount, int columnsCount, int **matrix) {
    for (register int r = 0; r < rowsCount; r++) {

        for (register int c = 0; c < columnsCount; c++) {

            cin >> matrix[r][c];
        }

        cout << endl;
    }
}

void MatrixManager::printMatrix(int order, int **matrix) {

    this->printMatrix(order, order, matrix);
}

void MatrixManager::printMatrix(int rowsCount, int columnsCount, int **matrix) {
    for (register int r = 0; r < rowsCount; r++) {

        for (register int c = 0; c < columnsCount; c++) {

            cout << matrix[r][c] << " ";
        }

        cout << endl;
    }
}

void MatrixManager::deleteMatrix(int order, int **matrix) {
    this->deleteMatrix(order, order, matrix);
}

void MatrixManager::deleteMatrix(int rowsCount, int columnsCount, int **matrix) {
    if (matrix != NULL) {
        for (register int r = 0; r < rowsCount; r++) {

            delete matrix[r];
        }

        delete matrix;
    }
}