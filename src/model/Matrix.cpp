#include "Matrix.h"

Matrix::Matrix() {
}

Matrix::~Matrix() {
}

int Matrix::getColumnCount() {
    return this->columnCount;
}

void Matrix::setColumnCount(int columnCount) {
    this->columnCount = columnCount;
}

int Matrix::getRowCount() {
    return this->rowCount;
}

void Matrix::setRowCount(int rowCount) {
    this->rowCount = rowCount;
}

void Matrix::create() {
    matrix = new int *[this->getRowCount()];

    for (int r = 0; r < this->getRowCount(); r++) {

        matrix[r] = new int[this->getColumnCount()];

        for (int c = 0; c < this->getColumnCount(); c++) {

            matrix[r][c] = 0;
        }
    }
}

int **Matrix::getMatrix() {
    return this->matrix;
}