#ifndef MATRICES_RUNNER_H
#define MATRICES_RUNNER_H

#include "../model/Constants.h"
#include "../model/Matrix.h"
#include "../model/MatrixManager.h"
#include "../model/MatrixOperations.h"
#include <iostream>

using namespace std;

class Runner {

private:

    Matrix *firstMatrix, *secondMatrix;
    MatrixManager *matrixManager;
    MatrixOperations *matrixOperations;

public:

    Runner();

    ~Runner();

    void run();

    void clearConsole();

    void showOptions();

    void breakLines();

    void readFirstMatrixSizes();

    void readSecondMatrixSizes();

    void readFirstMatrix();

    void readSecondMatrix();

    void showInvalidMatrixSizes();
};

#endif