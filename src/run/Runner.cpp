#include "Runner.h"

Runner::Runner() {
    this->firstMatrix = new Matrix();
    this->secondMatrix = new Matrix();
    this->matrixManager = new MatrixManager();
    this->matrixOperations = new MatrixOperations();

    this->matrixOperations->setFirstMatrix(this->firstMatrix);
    this->matrixOperations->setSecondMatrix(this->secondMatrix);
}

Runner::~Runner() {
    this->matrixManager->deleteMatrix(
            this->firstMatrix->getRowCount(),
            this->firstMatrix->getColumnCount(),
            this->firstMatrix->getMatrix()
    );
    this->matrixManager->deleteMatrix(
            this->secondMatrix->getRowCount(),
            this->secondMatrix->getColumnCount(),
            this->secondMatrix->getMatrix()
    );
    delete firstMatrix;
    delete secondMatrix;
    delete matrixManager;
    delete matrixOperations;
}

void Runner::run() {
    int option;

    do {

        this->showOptions();
        cin >> option;

        switch (option) {

            case Constants::Option::SUM:
                this->readFirstMatrixSizes();
                this->readSecondMatrixSizes();

                if (this->matrixOperations->canSum()) {
                    this->readFirstMatrix();
                    this->readSecondMatrix();
                    this->clearConsole();

                    cout << endl << "Sum of matrices is:" << endl;

                    int **resultMatrix = this->matrixOperations->getSum();

                    int rowsCount = this->firstMatrix->getRowCount();
                    int columnsCount = this->firstMatrix->getColumnCount();

                    this->matrixManager->printMatrix(rowsCount, columnsCount, resultMatrix);
                    this->matrixManager->deleteMatrix(rowsCount, columnsCount, resultMatrix);
                } else {

                    this->showInvalidMatrixSizes();
                }

                break;

            case Constants::Option::SUBTRACTION:
                this->readFirstMatrixSizes();
                this->readSecondMatrixSizes();

                if (this->matrixOperations->canSubtract()) {

                    this->readFirstMatrix();
                    this->readSecondMatrix();
                    this->clearConsole();

                    cout << endl << "Subtraction of matrices is:" << endl;

                    int **resultMatrix = this->matrixOperations->getSubtraction();

                    int rowsCount = this->firstMatrix->getRowCount();
                    int columnsCount = this->firstMatrix->getColumnCount();

                    this->matrixManager->printMatrix(rowsCount, columnsCount, resultMatrix);
                    this->matrixManager->deleteMatrix(rowsCount, columnsCount, resultMatrix);
                } else {

                    this->showInvalidMatrixSizes();
                }
                break;

            case Constants::Option::MULTIPLICATION:
                this->readFirstMatrixSizes();
                this->readSecondMatrixSizes();

                if (this->matrixOperations->canMultiply()) {

                    this->readFirstMatrix();
                    this->readSecondMatrix();
                    this->clearConsole();

                    cout << endl << "Multiplication of matrices is:" << endl;

                    int **resultMatrix = this->matrixOperations->getMultiplication();

                    int rowsCount = this->firstMatrix->getRowCount();
                    int columnsCount = this->secondMatrix->getColumnCount();

                    this->matrixManager->printMatrix(rowsCount, columnsCount, resultMatrix);
                    this->matrixManager->deleteMatrix(rowsCount, columnsCount, resultMatrix);
                } else {

                    this->showInvalidMatrixSizes();
                }
                break;

            case Constants::Option::INVERSE:
                int order;

                cout << "Type order of matrix: " << endl;
                cin >> order;
                this->firstMatrix->setRowCount(order);
                this->firstMatrix->setColumnCount(order);

                cout << "Type matrix" << endl;
                this->firstMatrix->create();
                this->matrixManager->readMatrix(order, order, this->firstMatrix->getMatrix());

                if (this->matrixOperations->canInverse()) {

                    int **resultMatrix = this->matrixOperations->getInversion();

                    this->matrixManager->printMatrix(order, resultMatrix);
                    this->matrixManager->deleteMatrix(order, resultMatrix);

                } else {
                    cout << "Dá não";
                }

                this->breakLines();
                break;

            case Constants::Option::EXIT:
                cout << endl << "Exiting..." << endl << "Thx :)";
                break;

            default:
                cout << endl << "Invalid option, please try again :(" << endl << endl;
                break;
        }

    } while (option != Constants::Option::EXIT);
}

void Runner::clearConsole() {
    system("clear");
}

void Runner::breakLines() {
    cout << endl << endl;
}

void Runner::showOptions() {
    cout << "------------------------------------" << endl;
    cout << "Some simple operations with matrices" << endl;
    cout << "------------------------------------" << endl;
    cout << Constants::Option::SUM << " - Sum" << endl;
    cout << Constants::Option::SUBTRACTION << " - Subtraction" << endl;
    cout << Constants::Option::MULTIPLICATION << " - Multiplication" << endl;
    cout << Constants::Option::INVERSE << " - Inversion" << endl;
    cout << Constants::Option::EXIT << " - Exit" << endl;
}

void Runner::readFirstMatrixSizes() {
    int aux;
    cout << "First Matrix" << endl << endl;

    cout << "Type rows count: " << endl;
    cin >> aux;
    this->firstMatrix->setRowCount(aux);

    cout << "Type columns count: " << endl;
    cin >> aux;
    this->firstMatrix->setColumnCount(aux);
    cout << endl;
}

void Runner::readSecondMatrixSizes() {
    int aux;
    cout << "Second Matrix" << endl << endl;

    cout << "Type rows count: " << endl;
    cin >> aux;
    this->secondMatrix->setRowCount(aux);

    cout << "Type columns count: " << endl;
    cin >> aux;
    this->secondMatrix->setColumnCount(aux);
    cout << endl;
}

void Runner::readFirstMatrix() {
    int rowsCount = this->firstMatrix->getRowCount();
    int columnsCount = this->firstMatrix->getColumnCount();

    cout << "Type first matrix" << endl;
    this->firstMatrix->create();
    this->matrixManager->readMatrix(rowsCount, columnsCount, this->firstMatrix->getMatrix());

    this->breakLines();
}

void Runner::readSecondMatrix() {
    int rowsCount = this->secondMatrix->getRowCount();
    int columnsCount = this->secondMatrix->getColumnCount();

    cout << "Type second matrix" << endl;
    this->secondMatrix->create();
    this->matrixManager->readMatrix(rowsCount, columnsCount, this->secondMatrix->getMatrix());
}

void Runner::showInvalidMatrixSizes() {
    cout << "Invalid sizes of matrices!" << endl;
    cout << endl;
    cout << "First matrix ->";
    cout << " rows: " << this->firstMatrix->getRowCount();
    cout << ", columns: " << this->firstMatrix->getColumnCount();
    cout << endl;
    cout << "Second matrix ->";
    cout << " rows: " << this->secondMatrix->getRowCount();
    cout << ", columns: " << this->secondMatrix->getColumnCount();
    cout << endl << endl;
}