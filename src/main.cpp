#include "run/Runner.h"

int main() {
    Runner *runner = new Runner();

    runner->run();

    delete runner;

    return EXIT_SUCCESS;
}