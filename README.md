# Matrices

Some simple operations with matrices

A school work.

![Matrices](./screenshots/matrices.png)

## References

- [Mundo Educação - Soma e subtração de matrizes](http://mundoeducacao.bol.uol.com.br/matematica/adicao-subtracao-matrizes.htm)
- [Mundo Educação - Multiplicação de matrizes](http://mundoeducacao.bol.uol.com.br/matematica/multiplicacao-matrizes.htm)
- [Mundo Educação - Cálculo da determinante de uma matriz](http://mundoeducacao.bol.uol.com.br/matematica/determinante-matriz-ordem-1-2-ou-3.htm)
